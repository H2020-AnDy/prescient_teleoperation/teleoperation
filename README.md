# Teleoperation

The controller to teleoperate the robot.

## Software requirements

On Ubuntu 16.04:
* docker_OpenSoT (https://gitlab.inria.fr/locolearn/docker_OpenSoT)
* teleoperation modules (https://gitlab.inria.fr/lpenco/teleoperation-modules)

On Windows: (if using the Xsens suit)
* Xsens-yarp streaming module (https://gitlab.inria.fr/H2020-AnDy/sensors.git)
* MVN Xsens

## Installation

```
git clone https://gitlab.inria.fr/lpenco/teleoperation.git
```
```
mkdir build && cd build
```
```
cmake ..
```
```
make
```


## How to run the teleoperation

- Launch a yarp server
```
yarpserver
```

- Run the *teleoperation-modules* you need (`retargeting` is required, unless the robot has the same kinematic and dynamic structure of the Xsens skeleton)

- If you are not using previously recorded sequences from *teleoperation-modules*, connect the Xsens suit and calibrate.

and on Windows:

- launch the *Xsens-yarp streaming module*,

- launch yarpmanager on Windows and open *.../sensors/yarp/app/scripts*,
```
yarpmanager
```
- launch the connection script to connect all the ports.


- Open gazebo and select the robot you want
```
gazebo
```

- Run the executable in *.../teleoperation/build/bin*
```
./teleoperation
```

- If using a dataplayer click on *play*




