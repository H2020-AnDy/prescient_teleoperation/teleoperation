#ifndef ICUBTELECONTROLLER_H_
#define ICUBTELECONTROLLER_H_

#include "RobotFramework/control/controller.hpp"
#include "RobotFramework/control/opensot_controller.hpp"
#include "RobotFramework/api/factory.hpp"
#include <math.h>
#include <yarp/os/BufferedPort.h>
#include <yarp/os/Bottle.h>
#include "yarp/os/Network.h"
#include "yarp/os/Log.h"

namespace control {

class teleop_controller : public opensot_controller {
 public:
  teleop_controller(const std::string &confFile, double dt);
  ~teleop_controller();

  void reset() override;
  bool run() override;

  void initAutoStack();
  void setStateMachine();

  void XsensComReconstruction();

  void postureTeleoperation();
  void baseTeleoperation();
  void comTeleoperation();
  void swingFootTeleoperation();
  void headTeleoperation();

  void handleXsensData(const Eigen::VectorXd &posRef, const Eigen::VectorXd &qRef);
  void handleOculusData(const Eigen::VectorXd &joystick);
  /* Retarget the Oculus data onto the robot*/
  void oculusRetarget();
  void handsRetarget();

  /*!
   *  \brief Function that streams the data to the LIP retargeting module
   */
  void streamLIPdata();
  /*!
   *  \brief Functions that get the corrected data from the IP corretor modules
   */
  void getLIPdata();
  /*!
   *  \brief Function that gets the data of the robot for the IP correctors
   */
  void getRobotData();

 private:

  bool  swingFoot_isRight = true;
  Eigen::Vector3d Rft_, Lft_;
  Eigen::Vector2d foot_size_ = Eigen::Vector2d(0.151, 0.058);
  // Task kinematics variables
  KDL::Frame base_frame_;
  KDL::Vector com_init_pose_;
  KDL::Frame swing_foot_init_pose_, swing_foot_pose_;
  KDL::Frame stance_foot_init_pose_, stance_foot_pose_;
  KDL::Vector com_stance_init_distance_;

  // Tasks
  OpenSoT::tasks::velocity::Cartesian::Ptr waist_;
  OpenSoT::tasks::velocity::Postural::Ptr postural_;
  OpenSoT::tasks::velocity::CoM::Ptr com_;
  OpenSoT::tasks::velocity::Cartesian::Ptr neck_;
  OpenSoT::tasks::velocity::Cartesian::Ptr swing_foot_, stance_foot_;

  // Task trajectories
  std::shared_ptr<trajectory_utils::trajectory_generator> trjGenerator_com_;
  std::shared_ptr<trajectory_utils::trajectory_generator> trjGenerator_swing_foot_;

  // SubTasks
  OpenSoT::SubTask::Ptr waist_sub;
  OpenSoT::SubTask::Ptr com_sub;
  OpenSoT::SubTask::Ptr headj_sub;
  OpenSoT::SubTask::Ptr l_armj_sub;
  OpenSoT::SubTask::Ptr r_armj_sub;
  OpenSoT::SubTask::Ptr torsoj_sub;
  OpenSoT::SubTask::Ptr l_legj_sub;
  OpenSoT::SubTask::Ptr r_legj_sub;
  OpenSoT::SubTask::Ptr neck_sub;

  std::list<unsigned int> waist_id;
  std::list<unsigned int> com_id;
  std::list<unsigned int> position_id;
  std::list<unsigned int> orientation_id;
  std::list<unsigned int> head_id;
  std::list<unsigned int> torso_id;
  std::list<unsigned int> l_arm_id;
  std::list<unsigned int> r_arm_id;
  std::list<unsigned int> l_leg_id;
  std::list<unsigned int> r_leg_id;

  // Limits
  OpenSoT::constraints::velocity::JointLimits::Ptr joint_lims_;
  OpenSoT::constraints::velocity::VelocityLimits::Ptr vel_lims_;

  int NumIterations_ = 100, countIteration_ = 0, simulationStep_ = 0;

  /////////////////// Postural Related /////////////////////////
   // Joint positions references constructed from retargeting reader
  Eigen::Vector3d neck_ref_;
  Eigen::Vector3d torso_ref_;
  Eigen::VectorXd l_arm_ref_;
  Eigen::VectorXd r_arm_ref_;
  Eigen::VectorXd l_leg_ref_;
  Eigen::VectorXd r_leg_ref_;

  // Joint positions variations references from retargeting reader
  Eigen::Vector3d neck_ref_delta;
  Eigen::Vector3d torso_ref_delta;
  Eigen::VectorXd l_arm_ref_delta;
  Eigen::VectorXd r_arm_ref_delta;
  Eigen::VectorXd l_leg_ref_delta;
  Eigen::VectorXd r_leg_ref_delta;  

  //initial posture vectors of each part of the robot
  Eigen::Vector3d neck0_;
  Eigen::Vector3d torso0_;
  Eigen::VectorXd l_arm0_;
  Eigen::VectorXd r_arm0_;
  Eigen::VectorXd l_leg0_;
  Eigen::VectorXd r_leg0_;

  //postural reference vector
  Eigen::VectorXd posturalReference;

  // initial q
  Eigen::VectorXd q_init;
  /////////////////////////////////////////////////////////////

  ////////////////// Xsens BS References //////////////////////
  // Relative body segment positions from retargeting reader
  Eigen::Vector3d waistHead_ref_;
  Eigen::Vector3d l_shoulderHand_ref_;
  Eigen::Vector3d r_shoulderHand_ref_;
  Eigen::Vector3d l_hipFoot_ref_;
  Eigen::Vector3d r_hipFoot_ref_;
  // global xSens feet position
  Eigen::Vector3d l_xsens_foot;
  Eigen::Vector3d r_xsens_foot;
  // z base
  double base_z_ref_;
  // quaternion base
  Eigen::VectorXd base_quat_ref_;
  // quaternion feet
  Eigen::VectorXd lf_quat_ref_;
  Eigen::VectorXd rf_quat_ref_;
  //quaternion neck 
  Eigen::VectorXd neck_quat_ref_;
  //quaternion chest 
  Eigen::VectorXd chest_quat_ref_;
  //feet offsets
  Eigen::VectorXd offset_feet;
  /////////////////////////////////////////////////////////

  /////////////////FINGERS FROM OCULUS////////////////////////
  // Port for writing fingers information
  yarp::os::BufferedPort<yarp::os::Bottle> hands_port_out;
  //flag to use the same button to close or open a hand
  bool isClosed_rHand = false;
  bool isClosed_lHand = false;
  ///////////////////////////////////////////////////////////

  ////////////////// Initial Values ///////////////////
  // initial positions
  Eigen::VectorXd baseInitPosition;
  // initial poses 
  Eigen::MatrixXd baseInitPose;
  Eigen::MatrixXd neckInitPose;
  Eigen::MatrixXd lf_swingInitPose;
  Eigen::MatrixXd rf_swingInitPose;
  Eigen::MatrixXd swingPose;
  ///////////////////////////////////////////////////////

  //////// Com References ////////
  // Human normalized offset for the CoM wrt to the feet position
  double offset_h;
  // Robot normalized offset for the CoM wrt to the feet position
  double offset_r;
  double offset_r_des = 0.5;
  double deltacomX_i;
  double deltacomX_start;
  // CoM references reconstructed from the offset
  Eigen::Vector3d old_CoM_ref_;
  Eigen::Vector3d delta_CoM_ref;
  Eigen::Vector2d comVel_ref_;
  ////////////////////////////////

  /////////// data for IP correctors ///////////
  bool firstRun_ = true;
  bool _3MMcorrection = false;
  // Port for reading and writing CoM information
  yarp::os::BufferedPort<yarp::os::Bottle> lip_port_in;
  yarp::os::BufferedPort<yarp::os::Bottle> lip_port_out;

  // input for LIP retargeting
  Eigen::Vector3d actual_CoM_;
  Eigen::Vector3d actual_dCoM_;
  Eigen::Vector3d actual_ddCoM_;
  Eigen::Vector3d actual_CoM_old;
  Eigen::Vector3d actual_dCoM_old;

  Eigen::Vector2d actual_CoP;

  Eigen::Vector2d comPos_des;
  Eigen::Vector2d comPos_des_old;
  Eigen::Vector2d comVel_des;
  Eigen::Vector2d comAcc_des;
  // output from LIP retargeting
  Eigen::Vector3d CoM_LIP;
  //center of support polygon
  Eigen::Vector2d Cglob;
  // support polygon bounds
  // x(Cglob frame) lower and upper bound
  Eigen::Vector2d x_Bound;
  // y(Cglob frame) lower and upper bound
  Eigen::Vector2d y_Bound;
  ////////////////////////////////////////////////////////

  ////////////// Oculus //////////////////////
  //oculus joysticks
  double stickX_left;
  double stickY_left;
  double buttonX;
  double buttonY;
  double buttonStick_left;
  double buttonFront_left;
  double buttonGrip_left;
  double stickX_right;
  double stickY_right;
  double buttonA;
  double buttonB;
  double buttonStick_right;
  double buttonFront_right;
  double buttonGrip_right;
  /////////////////////////////////////////////

};

}  // namespace control

#endif  // ICUBTELECONTROLLER_H_
