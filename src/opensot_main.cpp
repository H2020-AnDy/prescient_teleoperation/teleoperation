#include "iostream"

#undef USE_DART
#include "RobotFramework/api/robot_framework.hpp"

#include "csignal"

bool stop = false;

void signalHandler(int signum) {
  std::cout<<"Interrupt signal("<<signum<<") received. Stopping QP"<<std::endl;
  stop = true;
}

int main(int argc, char *argv[]) {
  signal(SIGINT, signalHandler);
  api::robot_framework rf("/home/icubuser/teleoperation/conf/icub_conf.yaml", &stop);
  while (!stop) {
    if (!rf.run()) {
      break;
    }
  }

  return 0;
}
