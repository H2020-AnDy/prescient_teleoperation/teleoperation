#include "teleop_controller.hpp"
using namespace yarp::os;

// namespace __teleop_controller {
// struct AutoRegister {
//   AutoRegister() {
//     api::Factory::instance().registerController("Icub_tele",
//       [](const std::string& fname, double dt){
//         return std::make_shared<control::teleop_controller>(fname, dt);
//       });
//   }
// };
//   static AutoRegister __x;
// }  // namespace __teleop_controller
RF_REGISTER_CONTROLLER("iCub_teleop", control::teleop_controller);

control::teleop_controller::teleop_controller(
  const std::string &confFile, double dt) : opensot_controller(confFile, dt),
  trjGenerator_com_(std::make_shared<trajectory_utils::trajectory_generator>(dt)),
  trjGenerator_swing_foot_(std::make_shared<trajectory_utils::trajectory_generator>(dt)) {
  q0_ << 0.0, 0.0, 0.4643, 0.0, 0.0, 0.0,
          0.20943, 0.07330167, 0.0244433, -0.349, -0.18849, -0.07079,
          0.20943, 0.07330167, 0.0244433, -0.349, -0.18849, -0.07079,
          0.087, 0.0, 0.0,
         -0.0506697, 0.227425, 0.00641583, 0.279253, -0.0239002, -0.00746833, 0.0260749,
          0.0171127, 0.0, 0.0,
         -0.0506697, 0.227425, 0.00641583, 0.279253, -0.0239002, -0.00746833, 0.0260749;      
}

control::teleop_controller::~teleop_controller() {
  LOG_WARNING("Controller teleop_controller Destructed");
}


void control::teleop_controller::reset() {

  LOG_INFO("Setting model:");
  base_frame_.p.x(q_(0));
  base_frame_.p.y(q_(1));
  base_frame_.p.z(q_(2));
  base_frame_.M =  base_frame_.M.RPY(q_(3), q_(4), q_(5));
  model_ptr_->setFloatingBasePose(base_frame_);
  std::cout << "BaseFrame: " << base_frame_.p.x() << " " << base_frame_.p.z() << std::endl;
  
  model_ptr_->setJointPosition(q0_);
  model_ptr_->setJointVelocity(dq0_);
  model_ptr_->setJointAcceleration(ddq_);

  model_ptr_->update();

  model_ptr_->computeNonlinearTerm(tauOffset_);

  // init center of mass task
  com_.reset(new OpenSoT::tasks::velocity::CoM(q0_, *model_ptr_));
  com_->setLambda(0.1);
  CoM_ = com_->getActualPosition();
  CoM_ref_ = com_->getActualPosition();
  LOG_INFO("--Com task initialized");
  std::cout << "COM: " << CoM_(0) << " " << CoM_(1) << " " << CoM_(2) << std::endl;

  // init floating base task
  waist_.reset(new OpenSoT::tasks::velocity::Cartesian("waist",
   q0_, *(model_ptr_.get()), "waist", "world"));
  // waist_->setOrientationErrorGain(0.1);
  waist_->setLambda(0.1);
  LOG_INFO("--Waist task initialized");
  // Get initial information of the task
  baseInitPose = waist_->getActualPose();
  //get position
  baseInitPosition = baseInitPose.block<3,1>(0,3);
  std::cout << "Waist: "<< baseInitPosition(0) << " " << baseInitPosition(1) << " " <<baseInitPosition(2) << std::endl;

  // init neck task
  neck_.reset(new OpenSoT::tasks::velocity::Cartesian("neck",
   q0_, *(model_ptr_.get()), "head", "world"));
  neck_->setOrientationErrorGain(0.1);
  LOG_INFO("--Neck task initialized");
  // Get initial information of the task
  neckInitPose = neck_->getActualPose();

  // init swing foot task
  if (swingFoot_isRight){
    swing_foot_.reset(new OpenSoT::tasks::velocity::Cartesian("swing_foot", q0_, *(model_ptr_.get()), "right_foot", "world"));
  }
  else {
    swing_foot_.reset(new OpenSoT::tasks::velocity::Cartesian("swing_foot", q0_, *(model_ptr_.get()), "left_foot", "world"));
  }
  swing_foot_->setOrientationErrorGain(1.);
  swing_foot_->setLambda(1.);
  LOG_INFO("--Swing foot task initialized");

  // init stance foot task
  if (swingFoot_isRight){
    stance_foot_.reset(new OpenSoT::tasks::velocity::Cartesian("stance_foot", q0_, *(model_ptr_.get()), "left_foot", "world"));
  }
  else{
    stance_foot_.reset(new OpenSoT::tasks::velocity::Cartesian("stance_foot", q0_, *(model_ptr_.get()), "right_foot", "world"));
  }
  stance_foot_->setOrientationErrorGain(1.0);
  stance_foot_->setLambda(1.0);
  LOG_INFO("--Stance foot task initialized");

  postural_.reset(new OpenSoT::tasks::velocity::Postural(q0_));
  postural_->setLambda(0.1); //(0.00000000000001);
  q_init = postural_->getReference();

  //Initial positions   
  com_init_pose_ = eigenToKDL(com_->getActualPosition());
  swing_foot_->getActualPose(swing_foot_init_pose_); 
  stance_foot_->getActualPose(stance_foot_init_pose_);

  //initial distance between measured foot pose frame and com position
  com_stance_init_distance_.x(com_init_pose_.x() - stance_foot_init_pose_.p.x());
  com_stance_init_distance_.y(com_init_pose_.y() - stance_foot_init_pose_.p.y());
  com_stance_init_distance_.z(com_init_pose_.z() - stance_foot_init_pose_.p.z());

  bool succ = true;

  Eigen::VectorXd qmin, qmax;
  model_ptr_->getJointLimits(qmin, qmax);
  joint_lims_.reset(new OpenSoT::constraints::velocity::JointLimits(q0_,
   qmax, qmin));
  // joint_lims_->setBoundScaling(0.5);
  LOG_INFO("Checking joints limits");
  for (int i = 0; i < q_.size(); ++i) {
    if(q_(i) < qmin(i)) {
      //succ = false;
      LOG_WARNING("q(" << i << ") inferior to the joint limit q = " <<
        q_(i) << " qmin = " << qmin(i));
    } else if(q_(i) > qmax(i)) {
      //succ = false;
      LOG_WARNING("q(" << i << ") superior to the joint limit q = " <<
        q_(i) << " qmax = " << qmax(i));
    } else {
      LOG_SUCCESS("q(" << i << ") = " << q_(i) << " OK");
    }
  }

  LOG_INFO("--Joints limits initialized");

  vel_lims_.reset(new OpenSoT::constraints::velocity::VelocityLimits(M_PI_4*2,
   dT_, q0_.size()));
  LOG_INFO("--Velocities limits task initialized");

  // LIP CoM stabilizer connection
  string portName = "/teleopCorrector";
  string qp_inPortName = portName + "/QPdata:i";
  string qp_outPortName = portName + "/QPdata:o";
  if (LIPstabilizer_){
    string lip_inoutPortName = portName + "/LIPdata";
    lip_port_in.open(qp_inPortName);
    lip_port_out.open(qp_outPortName);

    if(Network::connect(qp_outPortName, lip_inoutPortName))
      LOG_SUCCESS("QP->LIP communication established");
    if(Network::connect(lip_inoutPortName, qp_inPortName))
      LOG_SUCCESS("LIP->QP communication established");
  }
  
  // FingerController connection
  portName = "/hands_controller";
  qp_outPortName = portName + "/QPdata:o";
  if (handsControl_){
    string hands_inPortName = portName + "/q:i";
    hands_port_out.open(qp_outPortName);

    if(Network::connect(qp_outPortName, hands_inPortName))
      LOG_SUCCESS("QP->grasper communication established");
  }

  
  if (succ) {
    initAutoStack();
    isInitialized(true);
  }

  SM()->setState("idle");
  setStateMachine();
}


void control::teleop_controller::setStateMachine() {
  SM()->add("idle",
    [this]() {
      if (SM()->firstRun()) {
        LOG_INFO("idle state");
        //Do nothing
        SM()->setFirstrun(false);
      }

      if (streamingOculus_ && !streamingXsensRef_){
        SM()->setState("oculus_teleoperation");
        return true;
      } 
      else if (streamingXsensRef_) {
        SM()->setState("start_xsens_teleoperation");
        return true;
      }
      else {
        bool res = solve();
        return res;
      }
    });

  SM()->add("oculus_teleoperation",
    [this]() {
      if (SM()->firstRun()) {
        SM()->setFirstrun(false);
        LOG_INFO("oculus_teleoperation state");
      }

      if (!streamingOculus_) {
        SM()->setState("idle");
        return true;
      } else {
        // Organize the references from the oculus reader
        handleOculusData(oculus_joysticks_);
        oculusRetarget();

        bool res = solve();
        return res;
      }
    });


  SM()->add("start_xsens_teleoperation",
    [this]() {
      if (SM()->firstRun()) {
        SM()->setFirstrun(false);
        LOG_INFO("start_xsens_teleoperation state");
      }

      if (!streamingXsensRef_) { //teleoperation OFF
        SM()->setState("idle");
        return true;
      } else { //teleoperation ON
        //starting from double support mode
         SM()->setState("double_support");
         // SM()->setState("single_support");
        return true;
      }

      bool res = solve();
      return res;
    });


  SM()->add("double_support",
    [this]() {
      if (SM()->firstRun()) {
        swing_foot_->getActualPose(swing_foot_pose_);
        stance_foot_->getActualPose(stance_foot_pose_);

        SM()->setFirstrun(false);
        LOG_INFO("double_support state");
      }

      // Organize the references from the retargeting reader
      handleXsensData(bspos_xsens_ref_, q_xsens_ref_);
      // Update teleoperation tasks
      postureTeleoperation();
      if (streamingOculus_){
        handleOculusData(oculus_joysticks_);
      }
      if (handsControl_){
        handsRetarget();
      }
      baseTeleoperation();
      //Move CoM back between the feet
      CoM_(0) = (stance_foot_pose_.p.x() + swing_foot_pose_.p.x())/2 + com_stance_init_distance_.x();
      CoM_(1) = (stance_foot_pose_.p.y() + swing_foot_pose_.p.y())/2;
      comTeleoperation(); 
      //headTeleoperation();

      if (!streamingXsensRef_) {
        SM()->setState("idle");
        return true;
      }  
      else{
        bool res = solve();
        return res;
      }
    });

}


void control::teleop_controller::initAutoStack() {
  // Selection lists for the subtasks
  waist_id.push_back(2);
  waist_id.push_back(3);
  waist_id.push_back(4);
  waist_id.push_back(5);
  
  com_id.push_back(0);
  com_id.push_back(1);
  
  position_id.push_back(0);
  position_id.push_back(1);
  position_id.push_back(2);

  orientation_id.push_back(3);
  orientation_id.push_back(4);
  orientation_id.push_back(5);

  
  head_id.push_back(model_ptr_->getDofIndex("neck_pitch"));
  head_id.push_back(model_ptr_->getDofIndex("neck_roll"));
  head_id.push_back(model_ptr_->getDofIndex("neck_yaw"));

  torso_id.push_back(model_ptr_->getDofIndex("torso_pitch"));
  torso_id.push_back(model_ptr_->getDofIndex("torso_roll"));
  torso_id.push_back(model_ptr_->getDofIndex("torso_yaw"));

  l_arm_id.push_back(model_ptr_->getDofIndex("l_shoulder_pitch"));
  l_arm_id.push_back(model_ptr_->getDofIndex("l_shoulder_roll"));
  l_arm_id.push_back(model_ptr_->getDofIndex("l_shoulder_yaw"));
  l_arm_id.push_back(model_ptr_->getDofIndex("l_elbow_joint"));
  l_arm_id.push_back(model_ptr_->getDofIndex("l_wrist_prosup"));
  l_arm_id.push_back(model_ptr_->getDofIndex("l_wrist_pitch"));
  l_arm_id.push_back(model_ptr_->getDofIndex("l_wrist_yaw"));
  
  r_arm_id.push_back(model_ptr_->getDofIndex("r_shoulder_pitch"));
  r_arm_id.push_back(model_ptr_->getDofIndex("r_shoulder_roll"));
  r_arm_id.push_back(model_ptr_->getDofIndex("r_shoulder_yaw"));
  r_arm_id.push_back(model_ptr_->getDofIndex("r_elbow_joint"));
  r_arm_id.push_back(model_ptr_->getDofIndex("r_wrist_prosup"));
  r_arm_id.push_back(model_ptr_->getDofIndex("r_wrist_pitch"));
  r_arm_id.push_back(model_ptr_->getDofIndex("r_wrist_yaw"));

  l_leg_id.push_back(model_ptr_->getDofIndex("l_hip_pitch"));
  l_leg_id.push_back(model_ptr_->getDofIndex("l_hip_roll"));
  l_leg_id.push_back(model_ptr_->getDofIndex("l_hip_yaw"));
  l_leg_id.push_back(model_ptr_->getDofIndex("l_knee"));
  l_leg_id.push_back(model_ptr_->getDofIndex("l_ankle_pitch"));
  l_leg_id.push_back(model_ptr_->getDofIndex("l_ankle_roll"));
  
  r_leg_id.push_back(model_ptr_->getDofIndex("r_hip_pitch"));
  r_leg_id.push_back(model_ptr_->getDofIndex("r_hip_roll"));
  r_leg_id.push_back(model_ptr_->getDofIndex("r_hip_yaw"));
  r_leg_id.push_back(model_ptr_->getDofIndex("r_knee"));
  r_leg_id.push_back(model_ptr_->getDofIndex("r_ankle_pitch"));
  r_leg_id.push_back(model_ptr_->getDofIndex("r_ankle_roll"));

  // Subtasks
  waist_sub.reset(new OpenSoT::SubTask(waist_, waist_id));
  com_sub.reset(new OpenSoT::SubTask(com_, com_id));

  headj_sub.reset(new OpenSoT::SubTask(postural_, head_id));
  l_armj_sub.reset(new OpenSoT::SubTask(postural_, l_arm_id));
  r_armj_sub.reset(new OpenSoT::SubTask(postural_, r_arm_id));
  torsoj_sub.reset(new OpenSoT::SubTask(postural_, torso_id));
  l_legj_sub.reset(new OpenSoT::SubTask(postural_, l_leg_id));
  r_legj_sub.reset(new OpenSoT::SubTask(postural_, r_leg_id));

  neck_sub.reset(new OpenSoT::SubTask(neck_, orientation_id));

  //=============AUTOSTACK=================
  auto_stack_ = (stance_foot_ + swing_foot_)/
                (waist_sub +  com_sub + neck_sub + headj_sub + torsoj_sub + l_armj_sub + r_armj_sub);
  
  neck_sub->setActive(false);

  auto_stack_ = auto_stack_  << joint_lims_ << vel_lims_;
  //=======================================

  auto_stack_->update(q0_);

  LOG_INFO("--AutoStack set");

  solver_.reset(new OpenSoT::solvers::iHQP(auto_stack_->getStack(),
      auto_stack_->getBounds(), 1e8,
      OpenSoT::solvers::solver_back_ends::qpOASES));

  LOG_INFO("--Solver reset");
}


bool control::teleop_controller::run() {
  return((*SM())());
}


//==================================== IP FUNCTIONS FOR COM/POSTURE CORRECTION ========================================
void control::teleop_controller::getRobotData() {
  KDL::Frame T_ref;
  model_ptr_->getPose("left_foot", "world", T_ref);
  Lft_ = kdlToEigen(T_ref.p);
  model_ptr_->getPose("right_foot", "world", T_ref);
  Rft_ = kdlToEigen(T_ref.p);

  // distance right-left feet
  Eigen::Vector2d RLft;
  RLft(0) = Rft_(0) - Lft_(0);
  RLft(1) = Rft_(1) - Lft_(1);

  // Transformation to frame centered in the Support Polygon
  Cglob(0) = RLft(0)/2 + Lft_(0);
  Cglob(1) = RLft(1)/2 + Lft_(1);
  // get support polygon bounds
  x_Bound(1) = abs(RLft(0)/2) + foot_size_(0)*(5/6.8);
  x_Bound(0) = -abs(RLft(0)/2) - foot_size_(0)*(1.8/6.8);
  y_Bound(1) = abs(RLft(1)/2) + foot_size_(1)/2;
  y_Bound(0) = -y_Bound(1);

  
  // Transform actual com
  actual_CoM_(0) = actual_CoM_(0) - Cglob(0);
  actual_CoM_(1) = actual_CoM_(1) - Cglob(1);

  // get com desired values
  comAcc_des.setZero();
  comPos_des(0) = CoM_(0) - Cglob(0);
  comPos_des(1) = CoM_(1) - Cglob(1);

  actual_CoP = getCoP(0);
  actual_CoP = actual_CoP - Cglob;
  
  if (firstRun_){
    comPos_des_old = comPos_des;
    actual_CoM_old = actual_CoM_;
    actual_dCoM_old = actual_dCoM_;
  }
  // get actual com velocity
  actual_dCoM_ = (actual_CoM_ - actual_CoM_old)/dT_;
  actual_CoM_old = actual_CoM_;

  //get actual com acceleration
  actual_ddCoM_ = (actual_dCoM_ - actual_dCoM_old)/dT_;
  actual_dCoM_old = actual_dCoM_;

  Eigen::Vector2d act_com(2);
  act_com << actual_CoM_(0),  actual_CoM_(1);
  //comVel_des = (comPos_des - act_com)/dT_;
  comVel_des.setZero();
  comPos_des_old = comPos_des;

  firstRun_ = false;
}

void control::teleop_controller::streamLIPdata() {
  Bottle& output = lip_port_out.prepare();
  output.clear();
  for (int i=0; i<2; i++){
    output.addDouble(comPos_des(i));
  }
  for (int i=0; i<2; i++){
    output.addDouble(comVel_des(i));
  }
  for (int i=0; i<2; i++){
    output.addDouble(comAcc_des(i));
  }
  for (int i=0; i<2; i++){
    output.addDouble(actual_CoM_(i));
  }
  for (int i=0; i<2; i++){
    output.addDouble(actual_dCoM_(i));
  }
  for (int i=0; i<2; i++){
    output.addDouble(actual_ddCoM_(i));
  }
  output.addDouble(x_Bound(0));
  output.addDouble(y_Bound(0));
  output.addDouble(x_Bound(1));
  output.addDouble(y_Bound(1));
  output.addDouble(actual_CoM_(2));
  output.addDouble(dT_);

  //printf("result: %s\n", output.toString().c_str());

  lip_port_out.write();
}


void control::teleop_controller::getLIPdata(){
  Bottle *input = lip_port_in.read(false);
  if(input!=NULL){  
    Eigen::VectorXd lip_data(input->size());
    for (int i=0; i<lip_data.size(); i++){
      lip_data(i) = input->get(i).asDouble();
    }
    CoM_LIP(0) = lip_data(0);
    CoM_LIP(1) = lip_data(1);
    CoM_LIP(2) = actual_CoM_(2);
    
    CoM_LIP(0) = CoM_LIP(0) + Cglob(0);
    CoM_LIP(1) = CoM_LIP(1) + Cglob(1);
  }
}
//=================================================================================================================================


//::::::::::::::::::::::::::::::::: TELEOPERATION FUNCTIONS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
void control::teleop_controller::postureTeleoperation() {

  //--------------------- Postural Task ---------------------------
  // Get the current joint position
  posturalReference = q_;
  //use reference from retargeting for the neck, torso and arms
  int firstlArmId = model_ptr_->getDofIndex("l_shoulder_pitch");
  int firstrArmId = model_ptr_->getDofIndex("r_shoulder_pitch");
  int firstNeckId = model_ptr_->getDofIndex("neck_pitch");
  int firstTorsoId = model_ptr_->getDofIndex("torso_pitch");
  int firstlLegId = model_ptr_->getDofIndex("l_hip_pitch");
  int firstrLegId = model_ptr_->getDofIndex("r_hip_pitch");

  //________ p_i_R = p_start_R + Delta_p_R _________
  int size = l_arm_ref_delta.size();
  l_arm0_ = q_init.segment(firstlArmId,size);
  r_arm0_ = q_init.segment(firstrArmId,size);
  l_arm_ref_ = l_arm0_ + l_arm_ref_delta;
  r_arm_ref_ = r_arm0_ + r_arm_ref_delta;
  // Eigen::Vector3d l_wrist_ref_ = l_arm_ref_.tail(3);
  // for (int i=0; i<3; i++){
  //   l_wrist_ref_(i)*=180/3.1415;
  // }
  // l_arm_ref_.tail(3) = l_wrist_ref_;
  // Eigen::Vector3d r_wrist_ref_ = r_arm_ref_.tail(3);
  // for (int i=0; i<3; i++){
  //   r_wrist_ref_(i)*=180/3.1415;
  // }
  // r_arm_ref_.tail(3) = r_wrist_ref_;

  size = l_leg_ref_delta.size();
  l_leg0_ = q_init.segment(firstlLegId,size);
  r_leg0_ = q_init.segment(firstrLegId,size);
  l_leg_ref_ = l_leg0_ + l_leg_ref_delta;
  r_leg_ref_ = r_leg0_ + r_leg_ref_delta;

  size = torso_ref_delta.size();
  //transfer some hip on the torso
  torso_ref_delta(0) += (l_leg_ref_delta(0)+r_leg_ref_delta(0))/8;
  // torso_ref_delta(1) += (l_leg_ref_delta(1)+r_leg_ref_delta(1))/8;
  // torso_ref_delta(2) += (l_leg_ref_delta(2)+r_leg_ref_delta(2))/8;
  torso0_ = q_init.segment(firstTorsoId,size);
  neck0_ = q_init.segment(firstNeckId,size);
  torso_ref_ = torso0_ + torso_ref_delta;
  neck_ref_ = neck0_ + neck_ref_delta;
  
  posturalReference.segment(firstlLegId,l_leg_ref_.size()) = l_leg_ref_;
  posturalReference.segment(firstrLegId,r_leg_ref_.size()) = r_leg_ref_;
  posturalReference.segment(firstlArmId,l_arm_ref_.size()) = l_arm_ref_;
  posturalReference.segment(firstrArmId,r_arm_ref_.size()) = r_arm_ref_;
  posturalReference.segment(firstNeckId,neck_ref_.size()) = neck_ref_;
  posturalReference.segment(firstTorsoId,torso_ref_.size()) = torso_ref_;


  // Update postural task
  postural_->setReference(posturalReference);
  //----------------------------------------------------------------
}

void control::teleop_controller::baseTeleoperation() {
  //-------------------------- Base Height Task ---------------------------
  // base_i_R = base_start_R + Delta_base_R
  Eigen::MatrixXd basePose = baseInitPose;

  //++++ z Position ++++
  //get position
  Eigen::VectorXd basePosition = basePose.block<3,1>(0,3);
  // Limit the squat motion not to break the robot and/or avoid falling retargeting
  if (base_z_ref_ <= squat_threshold){
       base_z_ref_ = squat_threshold;
  }
  basePosition(2) = baseInitPosition(2) + base_z_ref_;
  //std::cout << "baseZ: " << basePosition(2) << std::endl;

  // changing position reference in x,y,z directions
  basePose.block<3,1>(0,3) = basePosition;

  // Update base task
  waist_->setReference(basePose);
  //----------------------------------------------------------------
}

void control::teleop_controller::comTeleoperation() {
  //-------------------------- CoM Task ---------------------------- 
  //getCoM(q_rbdl_, dq_rbdl_, &actual_CoM_(0), &actual_CoM_(1), &actual_CoM_(2));

  // if(LIPstabilizer_){
  //   XsensComReconstruction();
  //   //std::cout << "offset_r: " << offset_r << std::endl;
  //   getRobotData();
  //   streamLIPdata();
  //   getLIPdata();
  // }

  // // Update CoM task
  // if (LIPstabilizer_) {
  //   com_->setReference(CoM_LIP);
  // }
  // else {
    com_->setReference(CoM_);
  // }
  //----------------------------------------------------------------
}

void control::teleop_controller::headTeleoperation(){
  //-------------------------- Head Orientation Task ---------------------------
  Eigen::MatrixXd neckPose = neckInitPose;
  //build Rotation matrix
  Eigen::MatrixXd Rotation_neck(3,3);

  //adjust xsens head frame to global frame of the robot (X forward, Z up)
  double tmpr = neck_quat_ref_(2);
  neck_quat_ref_(2) = neck_quat_ref_(3);
  neck_quat_ref_(3) = tmpr;

  Rotation_neck << 1-2*neck_quat_ref_(2)*neck_quat_ref_(2)-2*neck_quat_ref_(3)*neck_quat_ref_(3), 2*neck_quat_ref_(1)*neck_quat_ref_(2)-2*neck_quat_ref_(0)*neck_quat_ref_(3), 2*neck_quat_ref_(1)*neck_quat_ref_(3)+2*neck_quat_ref_(0)*neck_quat_ref_(2),
              2*neck_quat_ref_(1)*neck_quat_ref_(2)+2*neck_quat_ref_(0)*neck_quat_ref_(3), 1-2*neck_quat_ref_(1)*neck_quat_ref_(1)-2*neck_quat_ref_(3)*neck_quat_ref_(3), 2*neck_quat_ref_(2)*neck_quat_ref_(3)-2*neck_quat_ref_(0)*neck_quat_ref_(1),
              2*neck_quat_ref_(1)*neck_quat_ref_(3)-2*neck_quat_ref_(0)*neck_quat_ref_(2), 2*neck_quat_ref_(2)*neck_quat_ref_(3)+2*neck_quat_ref_(0)*neck_quat_ref_(1), 1-2*neck_quat_ref_(1)*neck_quat_ref_(1)-2*neck_quat_ref_(2)*neck_quat_ref_(2);
  
  Rotation_neck.transposeInPlace();

  Eigen::MatrixXd Rotation_neck_dot(3,3);
  //dot product Initial Rotation and Xsens Rotation
  for (int i = 0; i<3; i++){
    for (int j = 0; j<3; j++){
      Rotation_neck_dot(i,j) = neckPose(i,0)*Rotation_neck(0,j) + neckPose(i,1)*Rotation_neck(1,j) + neckPose(i,2)*Rotation_neck(2,j);
    }
  }

  //changing Rotation matrix reference
  neckPose.block<3,3>(0,0) = Rotation_neck_dot;

  // Update neck task
  neck_->setReference(neckPose); 
  //----------------------------------------------------------------
}

void control::teleop_controller::XsensComReconstruction(){
  // Get the normalized offset from xSens
  offset_r_des = offset_h;
  std::cout << "offset_r_des: " << offset_r_des << std::endl;

  KDL::Frame T_ref;
  model_ptr_->getPose("left_foot", "world", T_ref);
  Lft_ = kdlToEigen(T_ref.p);
  model_ptr_->getPose("right_foot", "world", T_ref);
  Rft_ = kdlToEigen(T_ref.p);
  
  //compute robot desired CoM 2D ground projection on the feet line from the retargeted normalized offset 
  CoM_ref_(0) = Lft_(0) + offset_r_des*(Rft_(0) - Lft_(0));
  CoM_ref_(1) = Lft_(1) + offset_r_des*(Rft_(1) - Lft_(1));
  // Compute robot real CoM offset 
  //2D actual CoM ground projection 
  Eigen::Vector3d CoM_actual;
  Eigen::Vector2d CoM_2;
  CoM_actual = com_->getActualPosition();
  CoM_2 << CoM_actual(0), CoM_actual(1);
  Eigen::Vector2d Rft_2;
  Eigen::Vector2d Lft_2;
  Rft_2 << Rft_(0), Rft_(1);
  Lft_2 << Lft_(0), Lft_(1);
  Eigen::Vector2d p_RLfeet = Rft_2 - Lft_2; 
  double norm = (p_RLfeet(0)*p_RLfeet(0) + p_RLfeet(1)*p_RLfeet(1));
  if (norm<0)
    norm *= -1;
  //robot actual offset
  offset_r = (CoM_2 - Lft_2).dot(Rft_2 - Lft_2)/norm;
}

void control::teleop_controller::handsRetarget() {
  Bottle& output = hands_port_out.prepare();
  output.clear();

  if (streamingOculus_){
    // JOYTSICKS FOR FINGERS
    //LEFT
    if (!buttonFront_left && !isClosed_lHand){
      output.addString("close_hand");
      isClosed_lHand = true;
    } 
    else if (!buttonFront_left && isClosed_lHand){
      output.addString("open_hand");
      isClosed_lHand = false;
    }
    else if(!buttonGrip_left){
      output.addString("pointing_hand");
    } 
    else {
      output.addString("null");
    }

    //RIGHT
    if (!buttonFront_right && !isClosed_rHand){
      output.addString("close_hand");
      isClosed_rHand = true;
    } 
    else if (!buttonFront_right && isClosed_rHand){
      output.addString("open_hand");
      isClosed_rHand = false;
    }
    else if(!buttonGrip_right){
      output.addString("pointing_hand");
    }
    else {
      output.addString("null");
    }
  }

  //XSENS FOR WRIST
  Eigen::Vector3d l_wrist_ref = l_arm_ref_.tail(3);
  Eigen::Vector3d r_wrist_ref = r_arm_ref_.tail(3);
  for (int i=0; i<3; i++){
    l_wrist_ref(i)= l_wrist_ref(i)*180/3.1415;
  }
  for (int i=0; i<3; i++){
    r_wrist_ref(i)= r_wrist_ref(i)*180/3.1415;
  }
  for (int i=0; i<3; i++){
    output.addDouble(l_wrist_ref(i));
  }
  for (int i=0; i<3; i++){
    output.addDouble(r_wrist_ref(i));
  }
  //printf("result: %s\n", output.toString().c_str());
  hands_port_out.write();
}

void control::teleop_controller::oculusRetarget(){
  // control head orientation
  Eigen::Vector3d neck_oculus;
  // get oculus head orientation
  for (int i=0; i<3; i++)
    neck_oculus(i) = oculus_orientation_ref(i);
  // trasform to eigen
  Eigen::VectorXd posturalReference = q_;
  // insert this value in the postural reference vector
  int firstNeckId = model_ptr_->getDofIndex("neck_pitch");
  posturalReference.segment(firstNeckId,neck_oculus.size()) = neck_oculus;

  // Update postural task
  postural_->setReference(posturalReference);
}
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

//++++++++++++++++++++++++++++++++ UTILITY FUNCTIONS ++++++++++++++++++++++++++++++++++++++++++++++++++
void control::teleop_controller::handleXsensData(const Eigen::VectorXd &posRef, const Eigen::VectorXd &qRef){
  l_arm_ref_delta.resize(7);
  r_arm_ref_delta.resize(7);
  l_leg_ref_delta.resize(6);
  r_leg_ref_delta.resize(6);
  
  base_quat_ref_.resize(4);
  neck_quat_ref_.resize(4);

  // for (int i=0; i<3; i++) {
  //   l_foot_delta(i) = posRef(i+15);
  //   r_foot_delta(i) = posRef(i+18);
  //   l_hand_delta(i) = posRef(i+21);
  //   r_hand_delta(i) = posRef(i+24);
  // }
  base_z_ref_ = posRef(27);
  for (int i=0; i<4; i++) {
    base_quat_ref_(i) = posRef(i+28);
  }
  // for (int i=0; i<4; i++) {
  //   lf_quat_ref_(i) = posRef(i+32);
  //   rf_quat_ref_(i) = posRef(i+36);
  // }
  for (int i=0; i<4; i++) {
    neck_quat_ref_(i) = posRef(i+40);
  }
  offset_h = com_xsens_ref_(3);
  
  for (int i=0; i<torso_ref_delta.size(); i++){
    torso_ref_delta(i) = qRef(i);
    neck_ref_delta(i) = qRef(i+torso_ref_delta.size());
  }
  for (int i=0; i<l_arm_ref_delta.size(); i++) {
    l_arm_ref_delta(i) = qRef(i+neck_ref_delta.size()+torso_ref_delta.size());
    r_arm_ref_delta(i) = qRef(i+neck_ref_delta.size()+torso_ref_delta.size()+l_arm_ref_delta.size());
  }
  for (int i=0; i<l_leg_ref_delta.size(); i++) {
    l_leg_ref_delta(i) = qRef(i+neck_ref_delta.size()+torso_ref_delta.size()+l_arm_ref_delta.size()+r_arm_ref_delta.size());
    r_leg_ref_delta(i) = qRef(i+neck_ref_delta.size()+torso_ref_delta.size()+l_arm_ref_delta.size()+r_arm_ref_delta.size()+l_leg_ref_delta.size());
  }
}


 void control::teleop_controller::handleOculusData(const Eigen::VectorXd &joystick){
    stickX_left = joystick(0);
    stickY_left = joystick(1);
    buttonX = joystick(2);
    buttonY = joystick(3);
    buttonStick_left = joystick(4);
    buttonFront_left = joystick(5);
    buttonGrip_left = joystick(6);
    stickX_right = joystick(7);
    stickY_right = joystick(8);
    buttonA = joystick(9);
    buttonB = joystick(10);
    buttonStick_right = joystick(11);
    buttonFront_right = joystick(12);
    buttonGrip_right = joystick(13);
 }
 //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
